defmodule :"Elixir.PrisonersDilemma.Repo.Migrations.Add player ids to Game" do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :player_a_id, references(:users, on_delete: :nothing)
      add :player_b_id, references(:users, on_delete: :nothing)
    end

    create constraint("games", :player_A_id_must_be_smaller, check: "player_a_id < player_b_id")
  end
end
