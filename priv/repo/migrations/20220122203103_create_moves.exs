defmodule PrisonersDilemma.Repo.Migrations.CreateMoves do
  use Ecto.Migration

  def change do
    create table(:moves) do
      add :move, :string
      add :round, :integer
      add :points, :integer
      add :game_id, references(:games, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:moves, [:game_id])
    create index(:moves, [:user_id])
  end
end
