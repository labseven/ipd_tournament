defmodule PrisonersDilemma.Repo.Migrations.CreateGames do
  use Ecto.Migration

  def change do
    create table(:games) do
      add :cur_round, :integer, default: 0

      timestamps()
    end
  end
end
