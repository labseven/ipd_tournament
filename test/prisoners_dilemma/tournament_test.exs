defmodule PrisonersDilemma.TournamentTest do
  use PrisonersDilemma.DataCase

  alias PrisonersDilemma.Tournament

  describe "games" do
    alias PrisonersDilemma.Tournament.Game

    import PrisonersDilemma.TournamentFixtures

    @invalid_attrs %{movesA: nil, movesB: nil, pointsA: nil, pointsB: nil, rounds: nil, status: nil}

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert Tournament.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Tournament.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      valid_attrs = %{movesA: [], movesB: [], pointsA: 42, pointsB: 42, rounds: 42, status: "some status"}

      assert {:ok, %Game{} = game} = Tournament.create_game(valid_attrs)
      assert game.movesA == []
      assert game.movesB == []
      assert game.pointsA == 42
      assert game.pointsB == 42
      assert game.rounds == 42
      assert game.status == "some status"
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tournament.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      update_attrs = %{movesA: [], movesB: [], pointsA: 43, pointsB: 43, rounds: 43, status: "some updated status"}

      assert {:ok, %Game{} = game} = Tournament.update_game(game, update_attrs)
      assert game.movesA == []
      assert game.movesB == []
      assert game.pointsA == 43
      assert game.pointsB == 43
      assert game.rounds == 43
      assert game.status == "some updated status"
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Tournament.update_game(game, @invalid_attrs)
      assert game == Tournament.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Tournament.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Tournament.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Tournament.change_game(game)
    end
  end

  describe "games" do
    alias PrisonersDilemma.Tournament.Game

    import PrisonersDilemma.TournamentFixtures

    @invalid_attrs %{moveA: nil, moveB: nil, pointsA: nil, pointsB: nil}

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert Tournament.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Tournament.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      valid_attrs = %{moveA: true, moveB: true, pointsA: 42, pointsB: 42}

      assert {:ok, %Game{} = game} = Tournament.create_game(valid_attrs)
      assert game.moveA == true
      assert game.moveB == true
      assert game.pointsA == 42
      assert game.pointsB == 42
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tournament.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      update_attrs = %{moveA: false, moveB: false, pointsA: 43, pointsB: 43}

      assert {:ok, %Game{} = game} = Tournament.update_game(game, update_attrs)
      assert game.moveA == false
      assert game.moveB == false
      assert game.pointsA == 43
      assert game.pointsB == 43
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Tournament.update_game(game, @invalid_attrs)
      assert game == Tournament.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Tournament.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Tournament.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Tournament.change_game(game)
    end
  end

  describe "games" do
    alias PrisonersDilemma.Tournament.Game

    import PrisonersDilemma.TournamentFixtures

    @invalid_attrs %{cur_round: nil}

    test "list_games/0 returns all games" do
      game = game_fixture()
      assert Tournament.list_games() == [game]
    end

    test "get_game!/1 returns the game with given id" do
      game = game_fixture()
      assert Tournament.get_game!(game.id) == game
    end

    test "create_game/1 with valid data creates a game" do
      valid_attrs = %{cur_round: 42}

      assert {:ok, %Game{} = game} = Tournament.create_game(valid_attrs)
      assert game.cur_round == 42
    end

    test "create_game/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tournament.create_game(@invalid_attrs)
    end

    test "update_game/2 with valid data updates the game" do
      game = game_fixture()
      update_attrs = %{cur_round: 43}

      assert {:ok, %Game{} = game} = Tournament.update_game(game, update_attrs)
      assert game.cur_round == 43
    end

    test "update_game/2 with invalid data returns error changeset" do
      game = game_fixture()
      assert {:error, %Ecto.Changeset{}} = Tournament.update_game(game, @invalid_attrs)
      assert game == Tournament.get_game!(game.id)
    end

    test "delete_game/1 deletes the game" do
      game = game_fixture()
      assert {:ok, %Game{}} = Tournament.delete_game(game)
      assert_raise Ecto.NoResultsError, fn -> Tournament.get_game!(game.id) end
    end

    test "change_game/1 returns a game changeset" do
      game = game_fixture()
      assert %Ecto.Changeset{} = Tournament.change_game(game)
    end
  end

  describe "moves" do
    alias PrisonersDilemma.Tournament.Move

    import PrisonersDilemma.TournamentFixtures

    @invalid_attrs %{move: nil, points: nil, round: nil}

    test "list_moves/0 returns all moves" do
      move = move_fixture()
      assert Tournament.list_moves() == [move]
    end

    test "get_move!/1 returns the move with given id" do
      move = move_fixture()
      assert Tournament.get_move!(move.id) == move
    end

    test "create_move/1 with valid data creates a move" do
      valid_attrs = %{move: "some move", points: 42, round: 42}

      assert {:ok, %Move{} = move} = Tournament.create_move(valid_attrs)
      assert move.move == "some move"
      assert move.points == 42
      assert move.round == 42
    end

    test "create_move/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tournament.create_move(@invalid_attrs)
    end

    test "update_move/2 with valid data updates the move" do
      move = move_fixture()
      update_attrs = %{move: "some updated move", points: 43, round: 43}

      assert {:ok, %Move{} = move} = Tournament.update_move(move, update_attrs)
      assert move.move == "some updated move"
      assert move.points == 43
      assert move.round == 43
    end

    test "update_move/2 with invalid data returns error changeset" do
      move = move_fixture()
      assert {:error, %Ecto.Changeset{}} = Tournament.update_move(move, @invalid_attrs)
      assert move == Tournament.get_move!(move.id)
    end

    test "delete_move/1 deletes the move" do
      move = move_fixture()
      assert {:ok, %Move{}} = Tournament.delete_move(move)
      assert_raise Ecto.NoResultsError, fn -> Tournament.get_move!(move.id) end
    end

    test "change_move/1 returns a move changeset" do
      move = move_fixture()
      assert %Ecto.Changeset{} = Tournament.change_move(move)
    end
  end
end
