defmodule PrisonersDilemma.TournamentFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PrisonersDilemma.Tournament` context.
  """

  @doc """
  Generate a game.
  """
  def game_fixture(attrs \\ %{}) do
    {:ok, game} =
      attrs
      |> Enum.into(%{
        cur_round: 42
      })
      |> PrisonersDilemma.Tournament.create_game()

    game
  end

  @doc """
  Generate a move.
  """
  def move_fixture(attrs \\ %{}) do
    {:ok, move} =
      attrs
      |> Enum.into(%{
        move: "some move",
        points: 42,
        round: 42
      })
      |> PrisonersDilemma.Tournament.create_move()

    move
  end
end
