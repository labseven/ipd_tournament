defmodule PrisonersDilemmaWeb.PageController do
  use PrisonersDilemmaWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
