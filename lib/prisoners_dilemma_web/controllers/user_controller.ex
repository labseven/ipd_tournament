defmodule PrisonersDilemmaWeb.UserController do
  use PrisonersDilemmaWeb, :controller

  alias PrisonersDilemma.Accounts
  alias PrisonersDilemma.Accounts.User

  alias PrisonersDilemma.Tournament

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def show(conn, %{"id" => id}) do
    user = Accounts.get_user!(id)
    games = Tournament.list_games(user)
    render(conn, "show.html", user: user, games: games)
  end
end
