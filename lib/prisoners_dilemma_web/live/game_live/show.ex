defmodule PrisonersDilemmaWeb.GameLive.Show do
  use PrisonersDilemmaWeb, :live_view

  alias PrisonersDilemma.Accounts
  alias PrisonersDilemma.Tournament

  @impl true

  def mount(%{"opponent_id" => opponent_id}, session, socket) do
    socket =
      socket
      |> assign_login(session)
      |> assign_new(:opponent, fn -> Accounts.get_user!(opponent_id) end)

    player = socket.assigns.current_user
    opponent = socket.assigns.opponent

    socket =
      socket
      |> assign(cur_move: Tournament.get_current_move(player.id, opponent.id))
      |> assign(game_history: Tournament.get_moves_by_players(player.id, opponent.id))

    {:ok, socket}
  end

  @impl true
  def handle_params(%{"opponent_id" => opponent_id}, _, socket) do
    {
      :noreply,
      socket
      |> assign(:page_title, page_title(socket.assigns.live_action, opponent_id))
      |> assign(:opponent, Accounts.get_user!(opponent_id))
      #  |> assign(:game_history, Tournament.get_game_history!())
    }
  end

  @impl true
  def handle_event(event, params, socket),
    do: PrisonersDilemmaWeb.GameLive.PlayGame.handle_event(event, params, socket)

  defp page_title(:show, id) do
    other_user = Accounts.get_user!(id)
    other_user.username
  end
end
