defmodule PrisonersDilemmaWeb.GameLive.PlayGame do
  use PrisonersDilemmaWeb, :live_component

  alias PrisonersDilemma.Tournament

  def handle_event(action, _params, socket) do
    move =
      Tournament.make_move(
        socket.assigns.current_user.id,
        socket.assigns.opponent.id,
        action
      )

    {:noreply, assign(socket, :cur_move, move)}
  end
end
