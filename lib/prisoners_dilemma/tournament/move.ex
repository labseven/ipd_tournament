defmodule PrisonersDilemma.Tournament.Move do
  use Ecto.Schema
  import Ecto.Changeset

  schema "moves" do
    field :move, Ecto.Enum, values: [:cooperate, :defect, :none], default: :none
    field :points, :integer
    field :round, :integer
    belongs_to :game, PrisonersDilemma.Tournament.Game
    belongs_to :user, PrisonersDilemma.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(move, attrs) do
    move
    |> cast(attrs, [:move, :round, :points, :user_id, :game_id])
    |> validate_required([:round, :user_id, :game_id])
  end
end
