defmodule PrisonersDilemma.Tournament.Game do
  use Ecto.Schema
  import Ecto.Changeset

  alias PrisonersDilemma.Accounts.User

  schema "games" do
    field :cur_round, :integer, default: 0
    belongs_to :player_a, User
    belongs_to :player_b, User

    has_many :moves, PrisonersDilemma.Tournament.Move
    many_to_many :players, User, join_through: "moves"

    timestamps()
  end

  @doc false
  def changeset(game, attrs) do
    game
    |> cast(attrs, [:cur_round, :player_a_id, :player_b_id])
    |> validate_required([:player_a_id, :player_b_id])
    |> validate_number(:player_a_id, less_than: :player_b_id)

    # |> validate_required([:cur_round])
  end
end
