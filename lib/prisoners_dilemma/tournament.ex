defmodule PrisonersDilemma.Tournament do
  @moduledoc """
  The Tournament context.
  """

  import Ecto.Query, warn: false
  alias PrisonersDilemma.Repo

  alias PrisonersDilemma.Tournament.Game

  @doc """
  Returns the list of games.

  ## Examples

      iex> list_games()
      [%Game{}, ...]

  """
  def list_games do
    from(u in Game,
      preload: [:player_a, :player_b]
    )
    |> Repo.all()
  end

  @doc """
  Gets a single game.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_game!(123)
      %Game{}

      iex> get_game!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game!(id), do: Repo.get!(Game, id)

  @doc """
  Creates a game.

  ## Examples

      iex> create_game(%{field: value})
      {:ok, %Game{}}

      iex> create_game(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game(attrs \\ %{}) do
    %Game{}
    |> Game.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a game.

  ## Examples

      iex> update_game(game, %{field: new_value})
      {:ok, %Game{}}

      iex> update_game(game, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game(%Game{} = game, attrs) do
    game
    |> Game.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a game.

  ## Examples

      iex> delete_game(game)
      {:ok, %Game{}}

      iex> delete_game(game)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game(%Game{} = game) do
    Repo.delete(game)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game changes.

  ## Examples

      iex> change_game(game)
      %Ecto.Changeset{data: %Game{}}

  """
  def change_game(%Game{} = game, attrs \\ %{}) do
    Game.changeset(game, attrs)
  end

  alias PrisonersDilemma.Tournament.Move

  @doc """
  Returns the list of moves.

  ## Examples

      iex> list_moves()
      [%Move{}, ...]

  """
  def list_moves do
    Repo.all(Move)
  end

  @doc """
  Gets a single move.

  Raises `Ecto.NoResultsError` if the Move does not exist.

  ## Examples

      iex> get_move!(123)
      %Move{}

      iex> get_move!(456)
      ** (Ecto.NoResultsError)

  """
  def get_move!(id), do: Repo.get!(Move, id)

  @doc """
  Creates a move.

  ## Examples

      iex> create_move(%{field: value})
      {:ok, %Move{}}

      iex> create_move(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_move(attrs \\ %{}) do
    %Move{}
    |> Move.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a move.

  ## Examples

      iex> update_move(move, %{field: new_value})
      {:ok, %Move{}}

      iex> update_move(move, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_move(%Move{} = move, attrs) do
    move
    |> Move.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a move.

  ## Examples

      iex> delete_move(move)
      {:ok, %Move{}}

      iex> delete_move(move)
      {:error, %Ecto.Changeset{}}

  """
  def delete_move(%Move{} = move) do
    Repo.delete(move)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking move changes.

  ## Examples

      iex> change_move(move)
      %Ecto.Changeset{data: %Move{}}

  """
  def change_move(%Move{} = move, attrs \\ %{}) do
    Move.changeset(move, attrs)
  end

  def get_game_by_players(player_a_id, player_b_id) do
    a = min(player_a_id, player_b_id)
    b = max(player_a_id, player_b_id)

    case Repo.one(from g in Game, where: g.player_a_id == ^a and g.player_b_id == ^b) do
      nil ->
        {:ok, game} = create_game(%{player_a_id: a, player_b_id: b})
        game

      game ->
        game
    end
  end

  def get_moves_by_players(player_a_id, player_b_id) do
    game = get_game_by_players(player_a_id, player_b_id)

    q =
      from m in Move,
        where: m.game_id == ^game.id,
        where: m.round < ^game.cur_round

    Repo.all(q)
    |> Enum.group_by(fn m -> m.round end)
    |> Enum.map(fn {_, moves} ->
      Enum.reduce(moves, %{}, fn m, acc -> Map.put(acc, m.user_id, m) end)
    end)
  end

  @doc """
  get_current_move(user_id, opponent_id)
  """
  @spec get_current_move(integer, integer) :: %Move{}
  def get_current_move(user_id, opponent_id) do
    game = get_game_by_players(user_id, opponent_id)

    q =
      from m in Move,
        join: g in assoc(m, :game),
        where: g.id == ^game.id,
        where: m.user_id == ^user_id and m.round == g.cur_round

    case(Repo.one(q)) do
      nil ->
        {:ok, move} = create_move(%{game_id: game.id, user_id: user_id, round: game.cur_round})
        move

      move ->
        move
    end
  end

  def make_move(user_id, opponent_id, action) do
    # update user move
    get_current_move(user_id, opponent_id)
    |> update_move(%{move: action})

    # check if round is over and resolve points
    check_game_round(user_id, opponent_id)
    |> Enum.find(fn move -> move.user_id == user_id end)
    |> IO.inspect(label: "make_move")
  end

  @points_matrix %{cooperate: %{cooperate: 3, defect: 0}, defect: %{cooperate: 5, defect: 1}}
  @spec round_points(:cooperate | :defect, :cooperate | :defect) :: integer
  def round_points(player_move, opponent_move), do: @points_matrix[player_move][opponent_move]

  def check_game_round(player_a_id, player_b_id) do
    a = min(player_a_id, player_b_id)
    b = max(player_a_id, player_b_id)

    q =
      from g in Game,
        where: g.player_a_id == ^a and g.player_b_id == ^b,
        join: m in assoc(g, :moves),
        where: m.round == g.cur_round,
        preload: [moves: m]

    game = Repo.one(q)

    case game.moves do
      [m1, m2] when m1.move in [:cooperate, :defect] and m2.move in [:cooperate, :defect] ->
        {:ok, m1} = update_move(m1, %{points: round_points(m1.move, m2.move)})
        {:ok, m2} = update_move(m2, %{points: round_points(m2.move, m1.move)})

        update_game(game, %{cur_round: game.cur_round + 1})
        [m1, m2]

      moves ->
        moves
    end
  end
end
