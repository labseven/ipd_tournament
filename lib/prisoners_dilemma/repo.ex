defmodule PrisonersDilemma.Repo do
  use Ecto.Repo,
    otp_app: :prisoners_dilemma,
    adapter: Ecto.Adapters.Postgres
end
